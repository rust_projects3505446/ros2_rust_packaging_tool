use std::{fs::File, io::Write};
pub mod utils;
enum BuildTypes {
    /// Define an enum to represent the different ros2 build types
    AmentCargo,
    AmentPython,
    AmentCmake,
    Cmake,
}
impl BuildTypes {
    fn string_to_enum(argument: &str) -> BuildTypes {
        match argument {
            "ament_cargo" | "AmentCargo" | "AMENT_CARGO" => BuildTypes::AmentCargo,
            "ament_python" | "AmentPython" | "AMENT_PYTHON" => BuildTypes::AmentPython,
            "ament_cmake" | "AmentCmake" | "AMENT_CMAKE" => BuildTypes::AmentCmake,
            "cmake" | "Cmake" | "CMAKE" => BuildTypes::Cmake,
            _ => BuildTypes::Cmake,
        }
    }
    fn enum_to_string(&self) -> String {
        match self {
            BuildTypes::AmentCargo => String::from("ament_cargo"),
            BuildTypes::AmentPython => String::from("ament_python"),
            BuildTypes::AmentCmake => String::from("ament_cmake"),
            _ => String::from("cmake"),
        }
    }
}
enum FlagTypes {
    BuildType(BuildTypes),
    Description(String),
    Dependencies(Vec<String>),
    Library(String),
}

struct CliArgs {
    cargo_args: utils::CargoArgs::CargoArgs,
    arg: String,
}
impl CliArgs {
    fn input_handling() -> CliArgs {
        let args: Vec<String> = std::env::args().collect();
        CliArgs {
            cargo_args: utils::CargoArgs::CargoArgs::string_to_enum(args[1].as_str()),
            arg: args[2].clone(),
        }
    }
}
struct FileContent {
    content: String,
    name: String,
}
impl FileContent {
    fn file_writer(&self) -> std::io::Result<()> {
        let mut f = File::create(&self.name)?;
        f.write_all(&self.content.as_bytes())?;
        Ok(())
    }
    pub fn ros2_package_xml_builder(
        package_format: i32,
        package_name: &str,
        maintainer_email: &str,
        maintainer_name: &str,
        license_declaration: &str,
        build_type: BuildTypes,
        description: &str,
        dependecies: Vec<String>,
    ) -> String {
        [
            format!(r#"<package format="{:?}">"#, package_format),
            format!(r#"  <name>{}</name>"#, package_name),
            format!(r#"  <version>0.0.0</version>"#),
            format!(r#"  <description>{}</description>"#, description),
            format!(
                r#"  <maintainer email="{}">{}</maintainer>"#,
                maintainer_email, maintainer_name
            ),
            format!(r#"  <license>{}</license>"#, license_declaration),
            dependecies
                .iter()
                .map(|dependency| format!("  <depend>{}</depend>\n", dependency))
                .collect::<String>(),
            format!(r#"  <export>"#),
            format!(
                r#"    <build_type>{}</build_type>"#,
                build_type.enum_to_string()
            ),
            format!(r#"  </export>"#),
            format!(r#"</package>"#),
        ]
        .to_vec()
        .join("\n")
    }
}
/// Method that creates the for ros2 important package.xml
fn output_handling(args: CliArgs) -> () {
    args.cargo_args.enum_to_match(args.arg);
}

fn main() -> () {
    output_handling(CliArgs::input_handling())
}
