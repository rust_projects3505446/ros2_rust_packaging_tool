use crate::{BuildTypes, FileContent};

use std::{print, process::Command};
pub enum CargoArgs {
    Bench,
    Build,
    Check,
    Help,
    New,
    /// Creating a new package
    Run,
    Version,
}
impl CargoArgs {
    /// Implement methods for the CargoArgs enum
    ///Convert a string argument to the corresponding CargoArgs enum value
    /// Convert a string argument to the corresponding CargoArgs enum value
    /// Default to Help if the argument is not recognized
    pub fn string_to_enum(argument: &str) -> CargoArgs {
        match argument {
            "bench" | "Bench" | "BENCH" => CargoArgs::Bench,
            "build" | "Build" | "BUILD" | "B" | "b" => CargoArgs::Build,
            "check" | "Check" | "CHECK" => CargoArgs::Check,
            "help" | "Help" | "HELP" => CargoArgs::Help,
            "new" | "New" | "NEW" => CargoArgs::New,
            "run" | "Run" | "RUN" | "R" | "r" => CargoArgs::Run,
            "version" | "Version" | "VERSION" => CargoArgs::Version,
            _ => CargoArgs::Help,
        }
    }
    /// Perform the corresponding action based on the CargoArgs enum value
    pub fn enum_to_match(&self, name: String) -> () {
        match self {
            CargoArgs::Bench => print!("bench"),
            CargoArgs::Build => CargoArgs::build_package(&name),
            CargoArgs::Check => print!("check"),
            CargoArgs::Help => print!("help"),
            CargoArgs::New => CargoArgs::create_new_package(&name),
            CargoArgs::Run => print!("run"),
            CargoArgs::Version => print!("version"),
        }
    }
    /// Create a new package with the specified name
    /// Execute the cargo new command to create the package
    /// Generate the package.xml file content
    pub fn create_new_package(name: &str) -> () {
        println!(
            "{:?}",
            Command::new("cargo")
                .arg("new")
                .arg(name)
                .output()
                .expect("Failed to execute command")
        );
        FileContent {
            content: FileContent::ros2_package_xml_builder(
                3,
                name,
                "user@todo.todo",
                "user",
                "TODO: License declaration",
                BuildTypes::AmentCargo,
                "test package",
                vec!["std_msgs".to_string(), "rclrs".to_string()],
            ),
            name: format!("{}/package.xml", name),
        }
        // Write the package.xml file content
        .file_writer()
        .expect("Failed to write package.xml");
    }
    /// Execute the cargo build command to build the package
    pub fn build_package(name: &str) -> () {
        println!(
            "{:?}",
            Command::new("cargo")
                .arg("build")
                .arg(name)
                .output()
                .expect("Failed to execute command")
        );
    }
}
