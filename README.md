# Ros2_rust_packaging_tool
A tool for creating ros2 rust packages.
## introduction
The `package.xml` it will add to your rust project will look like this:
```
<package format="3">
  <name>cool_package</name>
  <version>0.0.0</version>
  <description>test package</description>
  <maintainer email="user@todo.todo">user</maintainer>
  <license>TODO: License declaration</license>
  <depend>std_msgs</depend>
  <depend>rclrs</depend>

  <export>
    <build_type>ament_cargo</build_type>
  </export>
</package>
```

Please be aware that you may have other dependencies than those given here, so change them if that's the case. The tool only creates a `package.xml` with a basic structure. The `package_name` written in the `package.xml` depends on your input.
## Getting Started
First, you want to build the project and use it for yourself. First you need to clone the repository.

### Get the Program_data
```
git clone -b ROS2_package_creation https://gitlab.com/rust_projects3505446/ros2_rust_packaging_tool.git
```
### Compile The Project
`ROS2_package_creation` is the first stable release that provides a standard ros2 rust package. 
Now you need to compile the program to get an executable:
```
cargo build --release
```
You'll find your executable in the `/target/release/` folder as `ros2_rust_packaging_tool`.
### Install the executable globally:
```
sudo cp target/release/ros2_rust_packaging_tool /usr/local/bin/ros2_rust_packaging_tool
```
This will copy the executable file to the `/usr/local/bin` directory, which is part of 
the system's PATH environment variable.
### Update the PATH environment variable:
```
export PATH=$PATH:/usr/local/bin
```
This will add the `/usr/local/bin` directory to your current PATH environment variable. For 
the change to be permanent, add this line to your `~/.bashrc` or `~/.bash_profile` file.
### trying it yourself

Now you can just type:
```
ros2_rust_packaging_tool new package_name
```
And the tool will create a fully functional ros2 rust package that's recognised by `colcon`, the ros2 build tool.
